/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import static com.app.Main.Main.buffer;
import static com.app.AppConstants.AppConstants.BUFFER_FULL;
import java.util.Random;

/**
 *
 * @author LAKSHMI BANSAL
 */
public class Producer extends Thread {

    private volatile boolean producerRunning = true;
    private Random r = new Random();

    @Override
    public void run() {
        while (producerRunning) {
            synchronized (buffer) {
                while (buffer.size() >= BUFFER_FULL) { // checking whether buffer is full or not
                    try {
                        System.out.println("producer waiting");
                        buffer.wait(); // wait if buffer is full
                    } catch (InterruptedException ex) {
                        System.out.println("Exception  : " + ex);
                    }
                }
                //if no if items are less than buffer capacity then add new items
                buffer.add(r.nextInt(50));
                System.out.println("added : " + buffer.get(buffer.size() - 1));
                if (buffer.size() == 1) {
                    buffer.notify(); //notify the consumer that item has been added if consumer is waiting
                }

            }

        }
    }

    public void shutdown() {
        producerRunning = false;
        
    }
}
