/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.entities;

import static com.app.Main.Main.buffer;
import static com.app.AppConstants.AppConstants.BUFFER_FULL;

/**
 *
 * @author LAKSHMI BANSAL
 */
public class Consumer extends Thread {

    private volatile boolean consumerRunning = true;

    @Override
    public void run() {
        while (consumerRunning) {
            synchronized (buffer) {
                while (buffer.isEmpty()) {
                    try {
                        System.out.println("consumer waiting ");
                        buffer.wait(); // if no item in the buffer then consumer will wait
                    } catch (InterruptedException ex) {
                        System.out.println("Exception  : " + ex);
                    }
                }
                //check whether there is an item in buffer or not
                System.out.println("Removed : " + buffer.remove(0));
                buffer.notify(); //notify the producer that item has been removed if producer is waiting
            }
        }
    }

    public void shutdown() {
        consumerRunning = false;
    }
}
