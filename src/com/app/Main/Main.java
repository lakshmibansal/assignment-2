/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.Main;

import java.util.*;

import com.app.entities.*;

/**
 *
 * @author LAKSHMI BANSAL
 */
public class Main {

    public static Scanner s = new Scanner(System.in);
    public static List<Integer> buffer = new ArrayList<>(); // Buffer to be shared

    public static void main(String[] args) {
     
        Producer producer = new Producer();
        Consumer consumer = new Consumer();

        producer.start();
        consumer.start();
        
        System.out.println("Press Enter To stop");
        s.nextLine();
        
        //stopping both threads
        producer.shutdown();
        consumer.shutdown();
        System.out.println("Producer and Consumer Stopped");
    }
}
